import axios from 'axios'

export const jiraConfigs = {
  endpoint: process.env.JIRA_ENDPOINT_URL || '',
  token: process.env.JIRA_API_TOKEN || '',
  username: process.env.JIRS_USERNAME || '',
  project: process.env.JIRA_PROJECT || '',
}

export const jiraApi = axios.create({
  baseURL: jiraConfigs.endpoint,
  auth: {
    username: jiraConfigs.username,
    password: jiraConfigs.token,
  },
})
