import { jiraApi } from '../configs/jira'
import { JiraIssue } from '../entities/jira-issue/JiraIssue'
import { IssueMapper } from '../mappers/jira/issuesMapper'

interface IRequest {
  project: string
  jql?: string
  startAt?: number
  maxResults?: number
  validateQuery?: 'strict' | 'warn' | 'none' | 'true' | 'false'
  fields?: string[]
  expand?: string
  properties?: string[]
  fieldsByKeys?: boolean
}

interface IResponse {
  expand: string
  startAt: number
  maxResults: number
  total: number
  issues: JiraIssue[]
}

class ListAllIssuesJira {
  async list(params: IRequest): Promise<IResponse> {
    const { data } = await jiraApi.get('search', {
      params,
    })

    const issues = IssueMapper.map(data.issues)

    return {
      expand: data.expand,
      startAt: data.startAt,
      maxResults: data.maxResults,
      total: data.total,
      issues,
    }
  }
}

export { ListAllIssuesJira }
