import { uuid } from 'uuidv4'
import { CronosIssue } from '../entities/cronos/CronosIssue'

class ListAllIssuesCronos {
  private issues: CronosIssue[] = []

  list(): CronosIssue[] {
    this.issues.push({
      id: uuid(),
      origin: 'cronos',
      props: {
        assigned: 'User1',
        author: 'Author1',
        createdAt: new Date(),
        description: 'Teste',
      },
    })

    this.issues.push({
      id: uuid(),
      origin: 'cronos',
      props: {
        assigned: 'User2',
        author: 'Author2',
        createdAt: new Date(),
        description: 'Teste',
      },
    })

    return this.issues
  }
}

export { ListAllIssuesCronos }
