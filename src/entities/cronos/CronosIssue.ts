import { Issue } from '../Issue'

interface Props {
  description: string
  author: string
  assigned: string
  createdAt: Date
}

export class CronosIssue extends Issue<Props> {
  private constructor(id: string, props: Props) {
    super({ origin: 'cronos', id, ...props })
  }
}
