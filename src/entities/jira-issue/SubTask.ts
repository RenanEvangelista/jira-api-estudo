import { IssueType } from './IssueType'
import { Priority } from './Priority'
import { Status } from './Status'

export interface SubTask {
  id: string
  key: string
  url: string
  fields: {
    summary: string
    status: Status
    priority?: Priority
    issueType: IssueType
  }
}
