import { AvatarUrl } from './AvatarUrl'

export interface Account {
  url: string
  accountId: string
  emailAddress: string
  avatarUrls: AvatarUrl
  displayName: string
  active: boolean
  timeZone: string
  accountType: string
}
