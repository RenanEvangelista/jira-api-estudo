import { Account } from './Account'
import { IssueType } from './IssueType'
import { Priority } from './Priority'
import { Project } from './Project'
import { Status } from './Status'
import { SubTask } from './SubTask'

export interface JiraIssueProps {
  id: string
  expand: string
  url: string
  key: string
  fields: {
    statuscategorychangedate: Date
    issueType: IssueType
    parent?: {
      id: string
      key: string
      url: string
      fields: {
        summary: string
        status: Status
        priority?: Priority
        issueType: IssueType
      }
    }

    timespent: number | null
    project: Project
    fixVersions: []
    resolution: boolean
    lastViewed: Date
    watches: {
      url: string
      watchCount: number
      isWatching: boolean
    }
    created: Date
    priority: Priority
    labels: []
    versions: []
    issuelinks: []
    assignee: Account
    updated: Date
    status: Status
    security: {
      url: string
      id: string
      description: string
      name: string
    }
    summary: string
    creator: Account
    subtasks: SubTask[]
    reporter: Account
    aggregateprogress: {
      progress: number
      total: number
    }
    progress: {
      progress: number
      total: number
    }
    votes: {
      url: string
      votes: number
      hasVoted: boolean
    }
  }
}
