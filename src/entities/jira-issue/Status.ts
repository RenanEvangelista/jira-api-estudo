export interface Status {
  url: string
  description: string
  iconUrl: string
  name: string
  id: string
  statusCategory: {
    url: string
    id: string
    key: string
    colorName: string
    name: string
  }
}
