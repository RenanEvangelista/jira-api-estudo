export interface Priority {
  url: string
  iconUrl: string
  name: string
  id: string
}
