import { AvatarUrl } from './AvatarUrl'

export interface Project {
  url: string
  id: string
  key: string
  name: string
  projectTypeKey: string
  simplified: boolean
  avatarUrls: AvatarUrl
}
