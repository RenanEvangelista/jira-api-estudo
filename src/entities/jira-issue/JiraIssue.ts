import { Issue } from '../Issue'
import { Account } from './Account'

import { JiraIssueProps } from './JiraIssueProps'

class JiraIssue extends Issue<JiraIssueProps> {
  get assignee(): Account {
    return this.props.fields.assignee
  }

  private constructor(props: JiraIssueProps) {
    super({ origin: 'jira', ...props })
  }
}

export { JiraIssue }
