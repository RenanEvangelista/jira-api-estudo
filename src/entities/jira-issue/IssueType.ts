export interface IssueType {
  url: string
  id: string
  description: string
  iconUrl: string
  name: string
  subtask: string | false
  avatarId: number
  hierarchyLevel: number
}
