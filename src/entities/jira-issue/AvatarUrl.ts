export interface AvatarUrl {
  '48': string
  '32': string
  '24': string
  '16': string
}
