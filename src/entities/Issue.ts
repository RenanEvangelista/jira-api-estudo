interface Props {
  id: string
  origin: 'jira' | 'cronos'
}

abstract class Issue<T> {
  id: string
  origin: 'jira' | 'cronos'
  props: T

  constructor(props: Props & T) {
    this.id = props.id
    this.origin = props.origin
    this.props = props
  }
}

export { Issue }
