import { Router } from 'express'
import { jiraConfigs } from '../../configs/jira'
import { ListAllIssuesCronos } from '../../services/ListAllIssuesCronos'
import { ListAllIssuesJira } from '../../services/ListAllIssuesJira'

export default (router: Router): void => {
  router.get('/list', async (_req, res) => {
    const JiraService = new ListAllIssuesJira()
    const cronosService = new ListAllIssuesCronos()
    const cronosIssues = cronosService.list()
    const jiraIssues = await JiraService.list({
      project: jiraConfigs.project,
    })

    res.json({
      jira: jiraIssues,
      cronos: cronosIssues,
    })
  })
}
