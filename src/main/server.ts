import 'dotenv/config'
import { app } from './config/app'
import { env } from '../configs/env'

app.listen(env.port, () => console.log(`Server running on ${env.port}`))
