import 'dotenv/config'
import { app } from './main/config/app'
import { env } from './configs/env'

app.listen(env.port, () => console.log(`server running on ${env.port}`))
